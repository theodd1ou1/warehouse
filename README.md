# warehouse

This experimental project is intended for learning Golang and microservice tech stacks only.

Stacks: golang, mongodb, grpc, docker, Google Cloud, Kubernetes, NATS, CircleCI, Terraform and go-micro.